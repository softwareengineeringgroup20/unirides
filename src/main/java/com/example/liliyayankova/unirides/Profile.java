package com.example.liliyayankova.unirides;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Profile extends AppCompatActivity {

    //TODO: layout and functionality of profile activity needs to be done!

    private boolean isDriver;

    private TextView text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        //assume the user is a passenger (isDriver = false) unless bundle extra userType value is DRIVER
        isDriver = false;

        /* checking if there are any bundle extras (should be the userType) */
        if(getIntent().getExtras() != null) {
            //setting userType to this result (DRIVER or PASSENGER)
            if(getIntent().getExtras().get("userType") == Login.userType.DRIVER){
                isDriver = true;
            }
        }

        //setting the user_toolbar layout as the action bar
        Toolbar toolbar = findViewById(R.id.toolbar);
        try {
            setSupportActionBar(toolbar);
        } catch (Exception e) {
            Log.e("TOOLBAR", e.toString());
        }

        text = ( TextView )findViewById( R.id.profile_text );

    }

    //Override for the user_toolbar menu items
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.toolbar_menu, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        //checks the id of the item, if the item id matches a case id then the code in that case block will execute
        switch(item.getItemId()){

            case(R.id.item_home):

                if (isDriver) {
                    Intent home_i = new Intent(Profile.this, Driver_Main.class);
                    startActivity(home_i);
                    return true;
                }else{
                    Intent home_i = new Intent(Profile.this, User_Main.class);
                    startActivity(home_i);
                    return true;
                }


            case(R.id.item_logout):

                /*
                TODO:
                    Possibly add a way to log out here? (not sure if we are even tracking the login instance
                    within each activity? as far as I understand the login just gets you to the next activity
                    however if you "logout" and just press the back button after it takes you back to the homepage
                    it will act as if you are still logged in when you reenter).
                    for now i have just set it so it goes back to the homepage (where you can register or login).
                 */

                Toast.makeText(Profile.this, "successfully logged out", Toast.LENGTH_SHORT).show();
                Intent logout_i = new Intent(Profile.this, Homepage.class);
                startActivity(logout_i);

                return true;

            case(R.id.item_profile):

                //do nothing (we're already in settings)
                return true;

            case(R.id.item_settings):

                Intent settings_i = new Intent(Profile.this, Settings.class);

                if(isDriver){
                    settings_i.putExtra("userType", Login.userType.DRIVER);
                }else{
                    settings_i.putExtra("userType", Login.userType.PASSENGER);
                }

                startActivity(settings_i);

                return true;

        }

        return super.onOptionsItemSelected(item);
    }
}
