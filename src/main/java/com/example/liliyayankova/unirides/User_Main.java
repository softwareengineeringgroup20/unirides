package com.example.liliyayankova.unirides;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

public class User_Main extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_main);

        //setting the user_toolbar layout as the action bar
        Toolbar toolbar = findViewById(R.id.toolbar);
        try {
            setSupportActionBar(toolbar);
        } catch (Exception e) {
            Log.e("TOOLBAR", e.toString());
        }

        ImageButton map_button = findViewById(R.id.view_route_button);
        map_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent map_i = new Intent(User_Main.this, Map.class);
                startActivity(map_i);
            }
        });

    }

    //Override for the user_toolbar menu items
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.toolbar_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        //checks the id of the item, if the item id matches a case id then the code in that case block will execute
        switch(item.getItemId()){

            case(R.id.item_home):

                //already home, so do nothing.
                return true;

            case(R.id.item_logout):

                /*
                TODO:
                    Possibly add a way to log out here? (not sure if we are even tracking the login instance
                    within each activity? as far as I understand the login just gets you to the next activity
                    however if you "logout" and just press the back button after it takes you back to the homepage
                    it will act as if you are still logged in when you reenter).
                    for now i have just set it so it goes back to the homepage (where you can register or login).
                 */

                Toast.makeText(User_Main.this, "successfully logged out", Toast.LENGTH_SHORT).show();
                Intent logout_i = new Intent(User_Main.this, Homepage.class);
                startActivity(logout_i);

                return true;

            case(R.id.item_profile):

                Intent profile_i = new Intent(User_Main.this, Profile.class);
                profile_i.putExtra("userType", Login.userType.PASSENGER);
                startActivity(profile_i);
                return true;

            case(R.id.item_settings):

                Intent settings_i = new Intent(User_Main.this, Settings.class);
                settings_i.putExtra("userType", Login.userType.PASSENGER);
                startActivity(settings_i);
                return true;

        }

        return super.onOptionsItemSelected(item);
    }
}
