package com.example.liliyayankova.unirides;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class Register extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        //setting the user_toolbar layout as the action bar
        Toolbar toolbar = findViewById(R.id.toolbar);
        try {
            setSupportActionBar(toolbar);
        } catch (Exception e) {
            Log.e("TOOLBAR", e.toString());
        }

        Button user_register = findViewById(R.id.user_signup);
        Button driver_register = findViewById(R.id.driver_signup);

        //user button listener
        user_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Register.this, User_Register.class);
                startActivity(i);
            }
        });

        //driver button listener
        driver_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Register.this, Driver_Register.class);
                startActivity(i);
            }
        });
    }

    //Override for the user_toolbar menu items
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.offline_toolbar_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        //checks the id of the item, if the item id matches a case id then the code in that case block will execute
        switch(item.getItemId()){

            case(R.id.item_settings):

                Intent settings_i = new Intent(Register.this, Settings.class);
                settings_i.putExtra("userType", Login.userType.NONE);
                startActivity(settings_i);
                return true;

        }

        return super.onOptionsItemSelected(item);
    }
}
