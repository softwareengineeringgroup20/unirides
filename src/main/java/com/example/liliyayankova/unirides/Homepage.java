package com.example.liliyayankova.unirides;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

public class Homepage extends AppCompatActivity {
    private TextView welcome;
    private TextView subheading;
    private Button login;
    private Button register;
    private ImageButton offline_map;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homepage);

        this.welcome = (TextView) findViewById(R.id.welcome);
        this.subheading = (TextView) findViewById(R.id.subh);
        this.login = (Button) findViewById(R.id.loginB);
        this.register = (Button) findViewById(R.id.registerB);
        this.offline_map = findViewById(R.id.offline_map);

        //setting the user_toolbar layout as the action bar
        Toolbar toolbar = findViewById(R.id.toolbar);
        try {
            setSupportActionBar(toolbar);
        } catch (Exception e) {
            Log.e("TOOLBAR", e.toString());
        }

        this.offline_map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent map_i = new Intent(Homepage.this, Offline_Map.class);
                startActivity(map_i);
            }
        });

        this.login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent signin = new Intent(Homepage.this, Login.class);
                signin.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(signin);
            }
        });

        this.register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent signup = new Intent(Homepage.this, Register.class);
                signup.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(signup);
            }
        });
    }

    //Override for the user_toolbar menu items
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.offline_toolbar_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        //checks the id of the item, if the item id matches a case id then the code in that case block will execute
        switch(item.getItemId()){

            case(R.id.item_settings):

                Intent settings_i = new Intent(Homepage.this, Settings.class);
                settings_i.putExtra("userType", Login.userType.NONE);
                startActivity(settings_i);
                return true;

        }

        return super.onOptionsItemSelected(item);
    }
}
