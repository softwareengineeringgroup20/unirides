package com.example.liliyayankova.unirides;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.ToggleButton;

public class Settings extends AppCompatActivity {

    private boolean isDriver;
    private boolean isLoggedIn;
    private boolean isSoundOn;

    private ToggleButton sound;
    private Spinner language;
    private Spinner save_location;
    private Button save_button;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        /*
         * assume the user is a passenger (isDriver = false) unless bundle extra userType value is DRIVER
         * also assume that the user is logged in until proven wrong
         */
        isDriver = false;
        isLoggedIn = true;

        /* checking if there are any bundle extras (should be the userType) */
        if(getIntent().getExtras() != null) {
            //setting userType to this result (DRIVER or PASSENGER)
            if(getIntent().getExtras().get("userType") == Login.userType.DRIVER){
                isDriver = true;
            }
            //if coming from the homepage (not signed in) set logged in to false
            if(getIntent().getExtras().get("userType") == Login.userType.NONE){
                isLoggedIn = false;
            }
        }

        //setting the user_toolbar layout as the action bar
        Toolbar toolbar = findViewById(R.id.toolbar);
        try {
            setSupportActionBar(toolbar);
        } catch (Exception e) {
            Log.e("TOOLBAR", e.toString());
        }

        this.language = findViewById(R.id.language_spinner);
        ArrayAdapter<CharSequence> l_adapter = ArrayAdapter.createFromResource(this,
                R.array.languages, android.R.layout.simple_spinner_item);
        l_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        language.setAdapter(l_adapter);

        this.save_location = findViewById(R.id.save_location_spinner);
        ArrayAdapter<CharSequence> s_adapter = ArrayAdapter.createFromResource(this,
                R.array.save_locations, android.R.layout.simple_spinner_item);
        s_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        save_location.setAdapter(s_adapter);

        this.sound = findViewById(R.id.sound_toggle);
        this.sound.setChecked(isSoundOn); // set the current state of a toggle button
        this.sound.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                //set isSoundOn to checked value (on or off)
                isSoundOn = isChecked;
            }
        });

        //if no settings file found, set to default values
        if(!loadSettings()){
            sound.setChecked(true);
            save_location.setSelection(0);
            language.setSelection(0);
        }


        this.save_button = findViewById(R.id.save_button);
        this.save_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //call method saveSettings
                if(saveSettings() == true) {

                    if (isLoggedIn) {
                        if (isDriver) {
                            Intent D_home_i = new Intent(Settings.this, Driver_Main.class);
                            startActivity(D_home_i);
                        } else {
                            Intent U_home_i = new Intent(Settings.this, User_Main.class);
                            startActivity(U_home_i);
                        }
                    } else {
                        Intent home_i = new Intent(Settings.this, Homepage.class);
                        startActivity(home_i);
                    }
                }else{
                    Toast.makeText(Settings.this, "Failed to save settings", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    //TODO: save settings to device or database
    public boolean saveSettings(){

        //if save is successful -> return true
        return true;

        //if not successful -> return false
    }

    //TODO: load settings from device or database
    public boolean loadSettings(){

        //if file found -> extract values -> return true

        //if no file found -> return false
        return  false;
    }

    //Override for the user_toolbar menu items
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        //setting toolbar options depending on user logged in (offline or online)
        if(isLoggedIn){
            MenuInflater menuInflater = getMenuInflater();
            menuInflater.inflate(R.menu.toolbar_menu, menu);
            return true;
        }else {
            MenuInflater menuInflater = getMenuInflater();
            menuInflater.inflate(R.menu.offline_toolbar_menu, menu);
            return true;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        /* checks if user is logged in, if not it shows a different toolbar */
        if(isLoggedIn) {
            //checks the id of the item, if the item id matches a case id then the code in that case block will execute
            switch (item.getItemId()) {

                case (R.id.item_home):

                    if (isDriver) {
                        Intent home_i = new Intent(Settings.this, Driver_Main.class);
                        startActivity(home_i);
                        return true;
                    } else {
                        Intent home_i = new Intent(Settings.this, User_Main.class);
                        startActivity(home_i);
                        return true;
                    }

                case (R.id.item_logout):

                /*
                TODO:
                    Possibly add a way to log out here? (not sure if we are even tracking the login instance
                    within each activity? as far as I understand the login just gets you to the next activity
                    however if you "logout" and just press the back button after it takes you back to the homepage
                    it will act as if you are still logged in when you reenter).
                    for now i have just set it so it goes back to the homepage (where you can register or login).
                 */

                    Toast.makeText(Settings.this, "successfully logged out", Toast.LENGTH_SHORT).show();
                    Intent logout_i = new Intent(Settings.this, Homepage.class);
                    startActivity(logout_i);

                    return true;

                case (R.id.item_profile):

                    Intent profile_i = new Intent(Settings.this, Profile.class);

                    if (isDriver) {
                        profile_i.putExtra("userType", Login.userType.DRIVER);
                    } else {
                        profile_i.putExtra("userType", Login.userType.PASSENGER);
                    }

                    startActivity(profile_i);

                    return true;

                case (R.id.item_settings):

                    //do nothing (already on the settings page)
                    return true;

            }

        }else{

            switch (item.getItemId()){

                case(R.id.item_settings):

                    //do nothing (already on the settings page)
                    return true;
            }
        }

        return super.onOptionsItemSelected(item);
    }
}
