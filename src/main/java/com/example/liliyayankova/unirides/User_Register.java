package com.example.liliyayankova.unirides;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class User_Register extends AppCompatActivity {
    private EditText forename;
    private EditText surname;
    private EditText email;
    private EditText password;
    private EditText age;
    private Button sign_up;

    /* Used for Firebase authorisation in creating a new account */
    private FirebaseAuth firebaseAuth;

    /* Allows access to the Firebase Database */
    private FirebaseDatabase firebaseDatabase;

    /* Used to add values to the database */
    private DatabaseReference dbReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_register);

        //setting the user_toolbar layout as the action bar
        Toolbar toolbar = findViewById(R.id.toolbar);
        try {
            setSupportActionBar(toolbar);
        } catch (Exception e) {
            Log.e("TOOLBAR", e.toString());
        }

        //makes it so the keyboard doesn't automatically popup!
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        this.forename = (EditText) findViewById(R.id.forename);
        this.surname = (EditText) findViewById(R.id.surname);
        this.email = (EditText) findViewById(R.id.email);
        this.password = (EditText) findViewById(R.id.password);
        this.age = (EditText) findViewById(R.id.age);
        this.sign_up = (Button) findViewById(R.id.signup_button);

        this.firebaseAuth = FirebaseAuth.getInstance();
        this.firebaseDatabase = FirebaseDatabase.getInstance();
        this.dbReference = firebaseDatabase.getReference();

        //sign_up button listener
        sign_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //execute register method
                registerClick(view);
            }
        });


    }

    public void registerClick(View v) {
        final String forenameText = forename.getText().toString().trim();
        final String surnameText = surname.getText().toString().trim();
        final String emailText = email.getText().toString().trim();
        final String passwordText = password.getText().toString().trim();
        final String ageText = age.getText().toString().trim();

        Log.d("Email", emailText);
        Log.d("Password", passwordText);

        if(TextUtils.isEmpty(forenameText) || TextUtils.isEmpty(surnameText)
        || TextUtils.isEmpty(emailText) || TextUtils.isEmpty(passwordText)
        || TextUtils.isEmpty(passwordText) || TextUtils.isEmpty(ageText)) {
            Toast.makeText(this, "One of the fields is empty!",Toast.LENGTH_SHORT).show();

        }

        else {
            firebaseAuth.createUserWithEmailAndPassword(emailText, passwordText)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if(task.isSuccessful()) {
                                FirebaseUser user = firebaseAuth.getCurrentUser();
                                String userId = user.getUid();
                                Toast.makeText(User_Register.this, forenameText,Toast.LENGTH_SHORT).show();

                                // Code to add values into the database
                                // Only temporary for now, might change these a bit later
                                dbReference.child("passengers").child(userId).child("first_name").setValue(forenameText);
                                dbReference.child("passengers").child(userId).child("last_name").setValue(surnameText);
                                dbReference.child("passengers").child(userId).child("age").setValue(ageText);
                                dbReference.child("passengers").child(userId).child("email").setValue(emailText);
                                dbReference.child("passengers").child(userId).child("average_rating").setValue(0.0);


                                //finish();
                                Toast.makeText(User_Register.this, "registration successful",Toast.LENGTH_SHORT).show();
                                Intent home = new Intent(User_Register.this, Homepage.class);
                                startActivity(home);

                            }
                            else {
                                Toast.makeText(User_Register.this, "registration not successful",Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
        }

    }

    //Override for the user_toolbar menu items
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.offline_toolbar_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        //checks the id of the item, if the item id matches a case id then the code in that case block will execute
        switch(item.getItemId()){

            case(R.id.item_settings):

                Intent settings_i = new Intent(User_Register.this, Settings.class);
                settings_i.putExtra("userType", Login.userType.NONE);
                startActivity(settings_i);
                return true;

        }

        return super.onOptionsItemSelected(item);
    }

}
